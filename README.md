# Docs

## Installing

Requires a `hugo_extended` version. [Releases page](https://github.com/gohugoio/hugo/releases).

## Writing

Hugo shortcodes: https://gohugo.io/content-management/shortcodes/

Theme-specific shortcodes: https://hugo-book-demo.netlify.app/docs/shortcodes/hints/

### Section numbering

VSCode extension "Markdown All in One" lets you generate section numbers. Put `<!-- omit in toc -->` next to headings you don't want numbers for.

## Development

### Install

```sh
git clone
cd docs
git submodule update
```

### Serve

```sh
hugo server --minify
```

## Build

```
hugo -d www --minify
zip www www/*
```