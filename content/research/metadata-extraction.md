---
title: Metadata Extraction
weight: 2
---

# Extraction of bibliographic metadata

Inspiring article: [ScienceBeam - using computer vision to extract PDF data](https://elifesciences.org/labs/5b56aff6/sciencebeam-using-computer-vision-to-extract-pdf-data)

## Academic papers on the subject

[CERMINE: automatic extraction of structured metadata from scientific literature](https://www.researchgate.net/publication/282495770_CERMINE_automatic_extraction_of_structured_metadata_from_scientific_literature)

### Code

[GROBID](https://github.com/kermitt2/grobid) - "a machine learning library for extracting, parsing and re-structuring raw documents such as PDF into structured XML/TEI encoded documents with a particular focus on technical and scientific publications."

## What about books?

Old and non-academic books are more critical. Academic papers are low-hanging fruit, since they're fairly standardized. Would we have a large enough corpus of scanned books to train an algorithm (or if such a pre-trained model already exists)?

Perhaps take EPUB standards and try to retrofit them?

Looking forward to the parsing engine so that we can standardize ISBN extraction amongst other things.

### Fiction and comics scraping

For LibGen core, fiction and comics are out of scope due to the massive amount of fundamental work that needs to be done at the backend.

The fiction section has additional requirements compared to nonfiction. Considering the amount of self-published titles that are churned out from Amazon, the community should be able to self-regulate to some degree, hence a volunteer editor persona.