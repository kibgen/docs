---
weight: 100
title: Research Problems
bookFlatSection: true
---

# Research Problems

This section is for any information gathering at an early stage when working with premature technologies.