---
weight: 10
---

# Social Philosophy

A federated format is designed to take advantage of both centralization and decentralization. It is important to foster a sustainable community. A community is the lifeblood of a project.

Perhaps the answer is to keep communities small and specialized. Maybe in the future, LibGen is improperly governed and becomes its own nightmare. When the majority flees to a new fork, the process starts anew.

## Publishing at LiFe

Just as scholars are directly uploading papers to SciHub, there is potential for LiFe to become a publishing destination, similar to ResearchGate and Academia.edu.

## Reference

- [Attacked from Within: What happens when a community scales?](https://atdt.freeshell.org/k5/story_2009_3_12_33338_3000.html)
- [And You Will Know Us by the Company We Keep: the design of social networks](https://www.eugenewei.com/blog/2021/9/29/and-you-will-know-us-by-the-company-we-keep)





