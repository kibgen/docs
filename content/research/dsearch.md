---
title: Distributed Search
weight: 1
---

# P2P Distributed Search

Right now dSearch technology is not so good yet. It's slower than centralized search, but it is less prone to censorship.

> Example: IPFS-based search [Dweb.page](https://pact.onl/ipfs/QmaG6gCPHBKEvQtCTG7ELa1J1fh9K7iitcCwrWponxFydy)

### References

- [Bringing Decentralized Search to Decentralized Services](https://www.cis.upenn.edu/~sga001/papers/desearch-osdi21.pdf)
