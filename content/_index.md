# LiFe Docs

Welcome to the Library Genesis Federation (LiFe).

The goal is to make Library Genesis [fully federated](https://en.wikipedia.org/wiki/Fediverse), hosted on an interoperable network sustained by the goodwill of volunteers. We would like to formalize the protocols and release the methodology for prosperity. LibGen was built on the shoulders of giants, owing its success to many projects before us which collected and distributed knowledge, and we will continue pass on this tradition.

> Example of a "federated Goodreads" -> [bookwyrm](https://joinbookwyrm.com/)

# Contribute

Library Genesis is for simple people with simple needs: to connect knowledge-seekers with knowledge-givers.

Anyone can contribute to the federation, both directly and indirectly.

> Join [the forums](https://libgen.life/viewtopic.php?p=379#p379) and the [Matrix room](https://matrix.to/#/#libgen:matrix.org). 👋 Say hello and introduce yourself!

## Academics and Professionals

### Professors

Push for [open education](https://opensource.com/resources/what-open-education) materials and incorporate them into your curriculums. Set the example for your pupils and peers.

### Scholars & Researchers

While LG may have difficulties with its status at present, the readers are allowed to legally use it under the fair use waiver. Use LG in research, communicate with our community to help you with data. Write theses on big data, search algorithms, library, technology, copy the collection for your research, and use LG for the common good! As
long as your use is fair, LG is yours.

### Advocates

Anyone who believes in the right of education, academics willing to defend our positions in a disclosed form, officially representing their schools and self, to organize meetings in the global referendum with us. We need academics who lobby the leading publishing houses and adjacent legal bodies, bring up current concerns, and discuss solutions. We need them to represent the solutions which we are ready to implement for the care of the scholarly community in its entirety.

Librarians and archivists can voice support for advancements in technology and better distribution models.

History and law professionals who will take our side, delve into the history of copyright and prepare a study first for internal use. At a later time to communicate propositions to society on a historical basis. We should clearly identify where the existing system is still adequate, and what part of it should be pruned, to argumentatively discuss these and adjacent problems in the open arena.

My strong belief is that LG will receive an unconditional social support and ultimately develop into a legitimate fully decentralized protected network not needing support beyond the natural tendency for humans to share.

## Hosting

1. IPFS backend and dWeb hosters; please, convey our needs on our behalf to hosting providers, ask them to pin the files on IPFS or donate long-term storage.

2. Cold storage providers ready for quick online replication in case of emergency. If you have spare storage facilities and good channels, you may host structured parts of the collection to help with replication.

## Scraping

Hunt down repositories of book data. Please post metadata sources [in this thread (may need to request access)](https://libgen.life/viewforum.php?f=49).

## Development

> **Software Design Philosophy**  
> LG is not for gurus. It is for simple people who may have great ideas. Keep it simple for everyone.

OS: We recommend that you use a Linux distro. Ubuntu 20.04 is a popular choice. Windows and Mac are full of spyware and we can't accomodate compatibility issues.

- [A brief introduction to Sci-Hub for developers](https://www.reddit.com/r/scihub/comments/nh5dbu/a_brief_introduction_to_scihub_for_developers/)

### Web Developers

Web and dWeb developers, including browser and browser add-on developers to include support to existing browsers.

The mainly used languages are PHP, Python, and MySQL.

All languages are accepted, since we are just volunteers and hobbyists after all. Some are working with Rust, ELK stack, golang, etc.

- Parsing: frequent work with regular expressions (regexp), building a generic parsing engine to monitor library state.

### IT and DevOps

Network infrastructure enthusiasts to work out a universal dWeb node.

Docker enthusiasts for setting up a full dWeb node without customizing OS. They would need to wrap up the existing dWeb methods to work as one, ideally without user intervention.

Custom distribution packagers for Linux, and other operating systems.

## Volunteer Custodians

In later years, LG enthusiasts and free librarians to handle the library tracker and submit community requests to the tracking system.
As volunteers and enthusiasts, we typically do not restrict ourselves with scheduled roadmaps. Instead, the groups would communicate to search the best solutions, and work on implementations upon the availability of resource.

### Documentation

The irony is that the more automation and conveniences are introduced, the more complex the systems become, and the more that needs to be documented.

- UX feedback and discussion

## Donations

