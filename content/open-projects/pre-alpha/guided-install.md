---
title: Guided Install
weight: 10
---

# Guided Install for dApps

Make it easy for volunteers to offer their hardware for decentralized hosting and running the network.

> I've been thinking of a generic way of supporting desired software projects by crowds, let's give it the code name `Generic helpers`. This could look as an OS application installer with check boxes to select projects to be actively supported by the resources available on the computer. If a project from a certified repository is selected, it is downloaded, installed, and run on its own to help the corresponding creators reach their goals by utilizing the host's resources. LG or online IP reporting services can be included as just special cases, but there can be hundreds and thousands of other crowd-sourced projects which, instead of asking for money and infrastructure, can directly use your resources without dealing with payments at all.
>
> I'd suggest we start such a project within LG, and later use it for crowd-support. Please, distribute this note in relevant circles to invite developers for this project on the open-source basis. I'd be happy to elaborate further and help during the development stage.

## Goals

- [ ] Create a CLI that analyzes the user's disk space and suggests an appropriate decentralized project
- [ ] Create a GUI once the CLI works well.

#### Example:

```
go-ipfs installed?	[✓]
Torrent installed?	[ ]
OS:			Linux
Free space: 		131435145 GB

Based on your system, here are the compatible projects:

[✓] Host an IPFS node and LibGen catalogues.
 └─ Ok, what content would you like to share?
(Arrow keys to select, Enter to toggle)

   Pre-defined catalogues:
   [✓] Sci-mag (1324 GB) <
   [✓] Fiction (9 GB)

   [ ] My own pins (3 GB)

   [Done] [Back]
```