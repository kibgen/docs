---
title: Pasing Engine
weight: 2
---

# Parsing Engine

There are many regexes used to extract data from various file formats. On a large scale, it's unsustainable to constantly flip between programming languages and redo implementations.

Ideally, implementation of the backend would be done in one language (historically, PHP). However, there are newer languages that perform certain tasks better, whether due to their design or ecosystem. For example, Rust is a C++ alternative for bare-metal code, and a well-optimized server keeps the cost cheap (as a volunteer organization, there's not a lot of money). Python is widespread in the AI/ML space to train models for automation and bulk data processing.

The reality is that volunteers are free to toy with any language they choose.

Hence, the order of operations are:

1) Standardize parsing implementations
2) Create parsers according to the standards

For example, use ANTL grammar files as a "source of truth" and to generate prototypes for quick hacks and testing in various target languages, which fulfills #1. After confirming the prototypes work, a parser can be hand-optimized, which fulfills #2.