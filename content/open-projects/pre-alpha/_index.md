---
title: Pre-alpha
bookFlatSection: true
weight: 50
---

# Pre-alpha

{{< hint "warning" >}}
**⚠ Warning**   
All topics in "pre-alpha" are experimental. Treat this as an unfinalized roadmap of where LiFe wants to go.
{{< /hint >}}

## Current priorities

### Redesign database

Until the database has been stabilized and confirmed, not much else can be done.

1. The next database schema must be finalized.
2. Separate schema development from application development. New applications developed hereafter will follow the new schema. Old applications are able to continue using the old schema.
3. When the schema is stable and proven with new apps, migrate old forks to the new schema and plug into the new app.
4. The old application can be ported if necessary.

### Security

Test

### Distributed systems management

#### Portable replication

VMs

#### Specialized OS

#### Deployment
