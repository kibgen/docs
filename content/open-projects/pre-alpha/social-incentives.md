---
title: Social Incentives
weight: 50
---

# Social Incentives

## Upvote/downvote files

## Save and share catalogues

The definition of *catalogue* is a collection of LG identification metadata. Currently, the IPFS pinning feature allows you to pin files that have been downloaded through IPFS. All pinned books in your personal library are already freely lent, but it's still cumbersome to find books and papers. People want to know what their friends are reading.

> A personalized library on demand. Imagine the power of this. It maximizes the motivation for random collectors to support their own partial LG mirrors. One of the possible applications is to let tutors form tiny libraries for their students and courses. Eventually a catalogue of such ready courses can be made available to the public for intense (self-)studies.

> we shall be onto making an interface for putting file together to make up such topical collections for separate distribution, kind of jubilee packs. Guys have made such a few times manually already. It would be great if a developer could have a look at designing a Web-form to:

1) pick specific books from our database(s), primarily books and papers;
2) form some soft of a package, like torrents or archive (with a delayed zipping or so);
3) publicizing it in the form of either an URL or torrent or magnet or whatever feasible output.

I don't have a better picture in mind right now, but the first idea that comes to mind is to employ the standard search with cookies remembering the selection direction from the search, and then a button in the search results "Publish" available only to admins or moderators (I can integrate the board functions later when the rest of the code is ready). It isn't difficult in this form.

If anybody has a better vision, please, put down your ideas and we shall brainstorm them.


## Staking, endorsing and tipping

LBRY has a similar model. Publishing model? Fun blockchain tokens?