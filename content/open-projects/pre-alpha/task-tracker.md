---
weight: 1
title: Task Tracker
---

# Task Tracker Dashboard

This will be a webapp to help Custodians and librarians manage submissions and error reports, similar to [Moksha's submissions tracker](https://youtu.be/cYKn4sfGHWg?t=80) (video starts at 1:20).

## User Experience (UX)

Define the UX of various personas. This job is for people who can prioritize requirements, draw up user flows, and navigate endless criticism.

It's too early to do UI-specific work at this stage. We need more research, planning and personnel. Still, advocates for the end-user are highly respected, to prevent development from running in circles.

## Automation, Analytics, Reporting

The backend of the task tracker, a "library with its own voice," needs to be implemented.

- Schedule, organize, and mark tasks for manual review
- Automatic archival of articles that follow proper bibliographic citation formats, and flagging items for manual review if they fail checks.
- Conversion scripts of various sorts

See [parsing engine](./parsing-engine.md).