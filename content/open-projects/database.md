---
title: Database Revamp
weight: 3
---

# Database Revamp

Since the establishment of the unified architecture, modifications over time lead to structural degradation, hampering collective
development. The major issues are:

1. No unified database: about 10 databases exist each with its own format
2. Each database is complemented with its own search and uploader codes
3. Search is not unified across sections; one needs to search in each section of interest
to exhaust the possibilities
4. Duplications in different LG sections cannot be universally handled by the default
database mechanism
5. Inconveniently uploaded material to one section cannot be reassigned to another
section. Point to care about: released torrents must be on sync with the actual
database records (after relocation). There should be no situation when the torrent points at a different section where the record was before reassignment.
1. Database formats change randomly. This is a quality control issue.
2. Databases are not easily modifiable (or extensible) to add, remove, or modify
columns (polymorphism)

All databases should be brought to a unified format with one leading table and a number of auxiliary relational tables to minimize repeated entities such as languages or rubrics.

## Polymorphism

1) A special section describing its polymorphic behaviour, i.e.
database queries and views, to make the code sufficiently independent of the
database structure to decouple the code versions from the database versions.

[need a diagram]

### Lead table

The leading table is extensible and accommodates common fields, while being able to specify other fields of the tables to merge. 

The unified leading table will be filled blockwise with the data of the merged tables. Such blockwise filling allows
to generalize the process of future database inclusions and modifications.

## Migration

To keep track of ingested forks,

3) Compatibility: indices of integrated databases are stored for robustness and the
ease of synchronization.

## Repository Types

There are several repositories serving different types of materials, and their number will only increase in the future. The total number of LibGen entries is over 100 million. It's no longer optimal for a file system to serve a random file in real time.

- Works, which may be a book, a paper, literature
  - A work may have multiple editions, variations, and versions, while conceptually still being the "same" work
  - A relation to all relevant files
- Cover images
  - A work may have several variations of covers
- File info
  - Filesize, format, hashes, and a pointer back to the human concept of a work, like a title
- Audit Logs
  - When people make changes to the catalogue, there needs to be a history.

The differences between the repositories of different content type will be eliminated
to unify the storage engine. The file versioning problem will be solved in creating parallel
repositories with sparse filling and an added iterator in the database... (append only list?)

## ID numbering and file structure

If to omit papers, there is no need to change the repository format. However, for adding papers it is better to add a folder level.

To make this conversion, developers should support the current and new format: 1000-folder-1000-file iteration as well as the 1000-folder-1000-folder-1000-file iteration.

Since the scheme only affects the ID, it is trivial to implement and host by only specifying a configuration constant.

### New ID scheme (v2)

Upgrade to 10^6 to make room for another subfolder.

![1000-folder-1000-folder-1000-file](/id-numbering-scheme-v2.svg)

```text
1853 000 000 # Top folder
├─ 1853 000 000/ # Subfolder
│  ├─ 1853000000 # File ID
│  ├─ 1853000001
│  ├─ 1853000...
│  └─ 1853000999
├─ 1853 001 000/
│  ├─ 1853001...
│  └─ 1853001999
├─ 1853 ... 000/
├─ 1853 998 000/
└─ 1853 999 000/

1854 000 000
├─ ...
```

### Current ID scheme (v1)

Files belong to the directory with their ID rounded down to the nearest 1000. See [the IPFS directory for an example](alpha.md#ipfs-directory-structure).

```text
1853000/  # Top folder
├─ 1853000  # File ID
├─ 1853001
├─ 1853002
├─   ...
├─ 1853998
└─ 1853999
```

## Separate data from rendering

The database will have behavior tables, with configuration entries telling the code how to deal with it, to keep the code unaware of the database implementation. This resembles virtualization in object-oriented programming via the abstract interfaces allowing a flexible database and repository linking to the code. It could resemble a CMS like [Directus](https://directus.io/), but simpler and lower maintenance, to borrow the idea of "database-mirroring".

Unified database carries all the structural information about itself and the site code does not need to be rewritten every time a field or entire section is added. Instead, query templates can be added or adapted in the database behaviour section without changing the code. Search and file integration are universal and only one uploader is needed for all cases (a section can be pointed out within).

For example, a search query in the code would first load the corresponding polymorphic parameters: tables and fields and output templates. Then would form SQL queries and shape out the results for the generic (database-alternation independent) code which would blindly display the results.