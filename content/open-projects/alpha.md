---
weight: 2
---

# Alpha Release

Requirements:

- [go-ipfs CLI](https://github.com/ipfs/go-ipfs)
- [SQLite 3](https://sqlite.org/index.html)

## SQLite Client

The "pocket" client which can be installed locally. Minimal with exact search capabilites.

Demo at [libgen.crypto](//libgen.crypto)

|Name|Source|
|------|---|
|Client | https://libgen-crypto.ipns.dweb.link/source.tar.gz|
|sqlite db |https://gateway.ipfs.io/ipns/libgen.crypto/libgen.sqlite3|

## Getting started

After extracting `source.tar.gz`, download the sqlite db and drop it in `data/libgen.sqlite3`, then run `make publish/`

### Generate sqlite db

1. Open `libgen-ipfs` and extract the latest "libgen_compact" database from [dbdumps](http://libgen.rs/dbdumps/) into the `data/` folder

Example using aria2 and unrar utilities:

```
cd data
aria2c http://libgen.rs/dbdumps/libgen_compact.rar
unrar e libgen_compact.rar
```

2. Run `full-build.sh` which sets up a Docker container.

## IPFS directory structure

The SQLite schema is a subset of the original database. It doesn't contain the CID of every book, but rather the CID of every directory `dir` of 1,000 books (one per torrent). Determine the directory number by taking the book's id and round down to the nearest 1000.

```sql
CREATE TABLE cids (
    dir  INTEGER PRIMARY KEY, 
    size INTEGER, 
    cid  TEXT
);
CREATE TABLE books (
    id         INTEGER PRIMARY KEY,
    title      TEXT    NOT NULL, 
    author     TEXT    NOT NULL,
    year       INTEGER NOT NULL,
    extension  TEXT    NOT NULL,
    filesize   INTEGER NOT NULL,
    md5        TEXT    NOT NULL,
    searchable INTEGER NOT NULL
);
```

For example, let's say we want to know the CID of the book "Shadow Libraries: Access to Knowledge in Global Higher Education". Run the following query:

```sh
sqlite> SELECT id, md5 FROM books WHERE title =
   ...> 'Shadow Libraries: Access to Knowledge in Global Higher Education';
id            md5                             
------------  --------------------------------
2217239       648edcb4d9529aa3c199a05723723583
2232438       108a7f4e752bdff81ee5b378d46bd9ce
```

There's two copies of the book. We'll just take the first; the id is 2217239 so the directory is 2217000.

```sh
sqlite> SELECT cid FROM cids WHERE dir = 2217000;
cid                                                              
-----------------------------------------------------------------
bafykbzaced5nurh26sso4andbdjcisgujiopyggne26d4pldnrlizjfpndotu   
```

To access this via ipfs, we need both the directory and the cid. From the ipfs command line, we can run:

```sh
ipfs get bafykbzaced5nurh26sso4andbdjcisgujiopyggne26d4pldnrlizjfpndotu/648edcb4d9529aa3c199a05723723583
```

Look at `web/src/index.ts` to see a more complete example.

*Source: [Where is catalog mapping metadata to IPFS?](https://www.reddit.com/r/libgen/comments/q8e9ch/where_is_catalog_mapping_metadata_to_ipfs/)*

## Troubleshooting

When running the redbean server, you may get this error:

```text
run-detectors: unable to find an interpreter for ./redbean-1.4.com
```

See [solution in this thread.](https://github.com/microsoft/WSL/issues/5466#issuecomment-695155716)
