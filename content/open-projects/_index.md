---
weight: 3
bookFlatSection: true
---

# Open Projects <!-- omit in toc -->

This page introduces the standards for discussing projects.

{{< hint "danger" >}}
**⛔ Blocker**   
There is no LibGen certified™ repository for active/completed projects
{{< /hint >}}

## Stages of a project <!-- omit in toc -->

1. Ideation and research
    - Discussion and debate over design choices, tradeoffs, and what goals will be served.
2. Proof-of-concept (POC)
    - A demo that works with the minimum requirements.
3. Alpha release
    - Early and crude for testing functionality.
4. Beta release
    - Resembles a finished product, typically needs to test scaling and load.
5. General release
    - The first version is considered done.

## Personas  <!-- omit in toc -->

These personas are defined for the sake of discussing project criteria. It is ordered from *general, less technical* to *specific, more technical*:

| Persona | Description |
|---------|-------------|
|Reader| A casual visitor who wants to search for books by name |
|Scholar|A researcher who can be more specific with their queries and is aware of citation/bibliographic quirks|
|Editor| A volunteer who wants to make small corrections to metadata, similar to Wikipedi's model |
|Custodian| A registered and endorsed user who makes organizational decisions for library content |
|Moderator | A registered and endorsed teammate who maintains discussion quality in communication channels |
|Community Developer| Someone who wants to scrap the database or use APIs, reliant on LibGen Core |
|Core Developer | Someone who works on the internals of LibGen |

## Related Projects

If LibGen isn't part of your calling, but you still want to help, check out others in the decentralized space:

- [IPFS](https://docs.ipfs.io/project/)
- [Decentralized VPNs](https://dvpnalliance.org/)
