---
weight: 2
---

# Current LibGen Database

The current system https://libgen.rs is maintained at https://forum.mhut.org

- List of resources: https://github.com/freereadorg/awesome-libgen 
- Desktop app docs: https://wiki.mhut.org/software:libgen_desktop?do=index

## Download Database Dumps

Dumps: http://libgen.rs/dbdumps/



{{< hint "info" >}}
**Note**   
Use a specialized downloader [such as aria2](https://aria2.github.io/) to prevent interruptions with large files.
{{< /hint >}}

<!-- To install `aria2`, extract the archive and compile:  
```sh
./configure
make install
```
The `INSTALL` file has more details.  

```sh
aria2c http://libgen.rs/dbdumps/fiction.rar
``` -->

## Import a database

**Requirement:** MySQL Community Server 5+

Start the MySQL server and login with admin credentials:

```sh
$ sudo systemctl start mysql
$ mysql -u root -p
Enter password: (Enter password)
```

Create a database and import dump:

```sh
mysql> CREATE DATABASE fiction;
mysql> SHOW DATABASES;
mysql> exit;
$ mysql -u root -p fiction < fiction.sql
```

Note that downloading the full databases and importing them will take a while.

## Trim database samples for testing

[Fast, hacky workaround (requires forum access permission)](https://libgen.life/viewtopic.php?p=80065#p80065)

## Schema

- [3rd party docs](https://gitlab.com/lucidhack/knowl/-/wikis/home)

Description of fields.

{{< tabs "db" >}}
{{< tab "hashes" >}}

```yaml
torrent: BitTorrent info hash. See [here for details](https://stackoverflow.com/questions/28348678/what-exactly-is-the-info-hash-in-a-torrent-file#28601408)
sha1: For integrity checks
sha256: For integrity checks
edonkey: [eDonkey identifier](https://en.wikipedia.org/wiki/Ed2k_URI_scheme)
crc32: Checksum for error-correction
aich: |-
  Advanced Intelligent Corruption Handling for eMule P2P.
  [See here for details](https://en.wikipedia.org/wiki/EMule#Basic_concepts)
btih: |-
  Torrent magnet link, bas32 encoded SHA1.
  See [Magnet URI specifications](http://magnet-uri.sourceforge.net/).
tth: [Tiger Tree Hash](https://en.wikipedia.org/wiki/Merkle_tree#Tiger_tree_hash)
md5: Unique identifier used for DB foreign keys. [Message-digest algorithm](https://en.wikipedia.org/wiki/MD5)
ipfs_cid: |-
  [IPFS content identifier](https://docs.ipfs.io/concepts/content-addressing/) which can use any [available hash](https://docs.ipfs.io/reference/cli/#ipfs-cid-hashes).
  Libgen uses `blake2b-256`.
```

{{< /tab >}}
{{< tab "Main Fields" >}}

Appears in `libgen.updated` and `fiction.libgen_fiction` 

```yaml
ID: file ID
###--- Bibliographic data
Title: title  
VolumeInfo: Volume number (in the title)  
Series: Series, with number  
Periodical: Journal, with number and year  
Author: Author's full name  
Year: Yeah
Edition: edition  
Publisher: - publisher
City: city where published  
###--- Bibliographic technical data
Pages: number of physical pages  
PagesInFile: Depends on the filetype
Language: language according to ISO 639
Topic: According to LibGen topics
Library: source code of the file
Issue: Library edition - Library field - dvd number or time when the book was added  
###--- identifiers
Identifier: ISBN or EAN (ISBN13)
ISNN: Identifier for periodicals  
ASIN: Amazon.com identifier  
UDC: (THIS FIELD IS MOSTLY EMPTY) Universal Decimal Classification
LBC: (THIS FIELD IS MOSTLY EMPTY) Library-Bibliographical Classification
DDC: Dewey Decimal Classification  
LCC: Library of Congress identifier  
Doi: digital object identifier  
Googlebookid: books.google.com identifier  
OpenLibraryID: [Internet Archive's book catalogue](https://openlibrary.org/)
###--- File options as a book
Commentary: comment on the quality of the file as a book, missing pages, etc.  
DPI: scan resolution  
Color: color  
Cleaned: Whether the pages have been cropped, straightened, and touched up  
Orientation: The orientation of the scan  
Paginated: cutting pages of a spread into sections  
Scanned: scanned  
# Vector: (NONEXISTENT) vector SVG assets
Bookmarked: Has a table of contents  
Searchable: has OCR layer (formerly called OCRpresence)  
###--- General file parameters
Filesize: size  
Extension: 
###--- Database service fields
Generic: md5 hash - reference to the best version of the file  
Filename: a link to the location of the file in the repository, if empty - the file is not in the repository  
Visible: whether it is visible in searches or banned for some reason  
Locator: the original name of the file before adding it to the repository  
Local: the presence of the file in user's local collection  
TimeAdded: date when the record was added  
TimeLastModified: date when record was last modified  
Coverurl: coverurl link  
Tags: Tags separated by semi-colon; like this;
IdentifierWODash: Identifier without dash
```
{{< /tab >}}
{{< tab "description" >}}

```yaml
###--- description
ID: serial number  
MD5: hash  
descr: description
TimeLastModified:
```

{{< /tab >}}
{{< /tabs >}}


### ID numbering

Files belong to the directory with their ID rounded down to the nearest 1000. See [the IPFS directory for an example](alpha.md#ipfs-directory-structure).

```text
1853000/  # Top folder
├─ 1853000  # File ID
├─ 1853001
├─ 1853002
├─   ...
├─ 1853998
└─ 1853999
```

